import { generateToken, deleteApplication } from './functions';

const APP_CLIENT_ID: string = Cypress.env('APP_CLIENT_ID');
const APP_CLIENT_SECRET: string = Cypress.env('APP_CLIENT_SECRET');

export const teardownEnvironment = () => {
  // token as application
  const role: string = 'system';
  const scope: string = 'applications:delete';

  generateToken(APP_CLIENT_ID, APP_CLIENT_SECRET, scope, role).then((token) => {
    Cypress.env('appToken', token);
  });

  // delete system application
  cy.then(() => {
    const appToken: string = Cypress.env('appToken');
    const clientId: string = Cypress.env('systemClientId');

    deleteApplication(appToken, clientId).then((response) => {
      expect(response.status).to.eq(204);
      console.log('Application deleted');
    });
  });
};
