import {
  generateToken, createApplication, createMerchant,
} from './functions';

const APP_CLIENT_SECRET: string = Cypress.env('APP_CLIENT_SECRET');
const APP_CLIENT_ID: string = Cypress.env('APP_CLIENT_ID');

export const setupEnvironment = () => {
  before(() => {
    // token as application
    let role: string = 'system';
    let scope: string = 'applications:create applications:secret';

    generateToken(APP_CLIENT_ID, APP_CLIENT_SECRET, scope, role).then((token) => {
      cy.wrap(token).should('not.be.empty');
      Cypress.env('appToken', token);
    });

    // system application
    cy.then(() => {
      role = 'system';
      const description: string = 'Cypress System Application';
      const scopeArray: string[] = ['applications:create', 'applications:secret'];
      const appToken: string = Cypress.env('appToken');

      createApplication(appToken, description, scopeArray, role).then((application) => {
        expect(application.id).to.not.be.undefined;
        Cypress.env('systemClientId', application.id);
      });
    });

    // partner application
    cy.then(() => {
      role = 'partner';
      const description: string = 'Cypress Partner Application';
      const scopeArray: string[] = ['merchants:create'];
      const appToken: string = Cypress.env('appToken');

      createApplication(appToken, description, scopeArray, role).then((application) => {
        expect(application.id).to.not.be.undefined;
        expect(application.sharedSecret).to.not.be.undefined;
        Cypress.env('partnerClientId', application.id);
        Cypress.env('partnerClientSecret', application.sharedSecret);
      });
    });

    // token as partner
    cy.then(() => {
      const clientId: string = Cypress.env('partnerClientId');
      const clientSecret: string = Cypress.env('partnerClientSecret');
      scope = 'merchants:create';
      role = 'partner';

      generateToken(clientId, clientSecret, scope, role).then((token) => {
        expect(token).to.not.be.undefined;
        cy.wrap(token).should('not.be.empty');
        Cypress.env('partnerToken', token);
      });
    });

    // create merchant
    cy.then(() => {
      const partnerToken: string = Cypress.env('partnerToken');
      const merchantName: string = `Merchant ${Math.floor(Math.random() * 1000)}`;

      createMerchant(partnerToken, merchantName).then((bpId) => {
        Cypress.env('merchantId', bpId);
      });
    });

    // create merchant application
    cy.then(() => {
      const description: string = 'Cypress Merchant Application';
      role = 'merchant';
      const scopeArray: string[] = ['addresses:create'];
      const appToken: string = Cypress.env('appToken');

      createApplication(appToken, description, scopeArray, role).then((application) => {
        expect(application.id).to.not.be.undefined;
        expect(application.sharedSecret).to.not.be.undefined;
        Cypress.env('merchantClientId', application.id);
        Cypress.env('merchantClientSecret', application.sharedSecret);
      });
    });

    // token as merchant
    cy.then(() => {
      const clientId: string = Cypress.env('merchantClientId');
      const clientSecret: string = Cypress.env('merchantClientSecret');
      scope = 'addresses:create';
      role = 'merchant';

      generateToken(clientId, clientSecret, scope, role).then((token) => {
        expect(token).to.not.be.undefined;
        cy.wrap(token).should('not.be.empty');
        Cypress.env('merchantToken', token);
      });
    });
  });
};
