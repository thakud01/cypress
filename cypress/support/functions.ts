import * as CryptoJS from 'crypto-js';
import * as uuid from 'uuid';
import { tokenPayload, applicationPayload } from './interfaces';

const ENVIRONMENT: string = Cypress.env('ENVIRONMENT');
const HOST: string = Cypress.env('HOST');

// Add fields to token generator, depending on the role provided
function addTokenFields(payload: tokenPayload, role: string) {
  const partnerId = Cypress.env('partnerId');
  const merchantId = Cypress.env('merchantId');

  switch (role) {
    case 'system':
      break;
    case 'partner':
      payload.partnerId = partnerId;
      break;
    case 'merchant':
      payload.merchantId = merchantId;
      break;
    default:
      throw new Error(`${role} does not exist`);
  }
}

// Add fields to application creator, depending on the role provided
function addApplicationFields(payload: applicationPayload, role: string) {
  const partnerId = Cypress.env('partnerId');
  const merchantId = Cypress.env('merchantId');

  switch (role) {
    case 'system':
      payload.defaultRole = 'system';
      payload.partnerId = partnerId;
      break;
    case 'partner':
      payload.defaultRole = 'partner';
      payload.partnerId = partnerId;
      break;
    case 'merchant':
      payload.defaultRole = 'merchant';
      payload.partnerId = partnerId;
      payload.merchantId = merchantId;
      break;
    default:
      throw new Error(`${role} does not exist`);
  }
}

// generates a token
export function generateToken(clientId: string, clientSecret: string, scope: string, role: string) {
  const partnerId = Cypress.env('partnerId');
  const signatureValue = uuid.v4();
  const timestamp = Math.floor(Date.now() / 1000);
  const hmac = CryptoJS.HmacSHA256(`${signatureValue}.${signatureValue.length}.${timestamp}`, clientSecret);
  const encodedSource = CryptoJS.enc.Base64.stringify(hmac);

  const payload: tokenPayload = {
    clientId,
    expiration: '30m',
    signatureValue,
    sub: role,
    timestamp,
    signature: encodedSource,
    audience: 'integration.api.authvia.com/v3',
    scope,
    partnerId,
  };

  addTokenFields(payload, role);

  return cy.request({
    method: 'POST',
    url: `https://${HOST}/v3/tokens`,
    body: payload,
    failOnStatusCode: false,
  }).then((response) => response.body.token);
}

// creates an application
export function createApplication(token: string, description: string, scope: string[], role: string) {
  const payload: applicationPayload = {
    description,
    expiration: {
      default: '1h',
      maximum: '1d',
    },
    scope,
    allowedAudiences: [`${ENVIRONMENT}.api.authvia.com/v3`],
  };

  addApplicationFields(payload, role);

  return cy.request({
    method: 'POST',
    url: `https://${HOST}/v3/applications`,
    body: payload,
    headers: {
      Authorization: `Bearer ${token}`,
    },
    failOnStatusCode: false,
  }).then((response) => {
    expect(response.status).to.eq(201);
    expect(response.body).to.contain.keys('id', 'sharedSecret');
    Cypress.env('id', response.body.id);
    Cypress.env('sharedSecret', response.body.sharedSecret);
    return { id: response.body.id, sharedSecret: response.body.sharedSecret };
  });
}

// deletes an application
export function deleteApplication(token: string, clientId: string) {
  return cy.request({
    method: 'DELETE',
    url: `https://${HOST}/v3/applications/${clientId}`,
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }).then((response) => {
    expect(response.status).to.eq(204);
    return response;
  });
}

// create a merchant
export function createMerchant(token: string, name: string) {
  return cy.request({
    method: 'POST',
    url: `https://${HOST}/v3/merchants`,
    headers: {
      Authorization: `Bearer ${token}`,
    },
    body: {
      profile: {
        name,
      },
    },
  }).then((response) => {
    expect(response.status).to.eq(201);
    return response.body.id;
  });
}
