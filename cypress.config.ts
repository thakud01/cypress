import { defineConfig } from "cypress";

export default defineConfig({
  reporter: 'cypress-mochawesome-reporter',
  reporterOptions: {
    reportFilename: '[name]-[timestamp]',
    reportDir: './reports',
    saveJson: true,
    saveHtml: false
  },
  e2e: {
    setupNodeEvents(on, config) {
      require('cypress-mochawesome-reporter/plugin')(on);
    },
    specPattern: 'testing/**/*.cy.{ts,tsx,js,jsx}'
  },
  env: {
    ENVIRONMENT: process.env.ENVIRONMENT,
    HOST: process.env.HOST,
    APP_CLIENT_ID: process.env.APP_CLIENT_ID,
    APP_CLIENT_SECRET: process.env.APP_CLIENT_SECRET,
    partnerId: process.env.partnerId
  }
});
